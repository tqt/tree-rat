#filter emptyLines

# LOCALIZATION NOTE: The 'en-US' strings in some URLs will be replaced with
# your locale code, and link to your translated pages as soon as they're live.

#define bookmarks_title Закладки
#define bookmarks_heading Закладки

#define personal_toolbarfolder Папка панели закладок

#define seamonkey_and_mozilla SeaMonkey и Mozilla

# LOCALIZATION NOTE (seamonkey):
# link title for https://www.seamonkey-project.org/ (in the personal toolbar)
#define seamonkey SeaMonkey

# LOCALIZATION NOTE (seamonkey_long):
# link title for https://www.seamonkey-project.org/ (in normal bookmarks)
#define seamonkey_long Проект SeaMonkey

# LOCALIZATION NOTE (mozilla_org_long):
# link title for http://www.mozilla.org/ (in normal bookmarks)
#define mozilla_org_long Организация Mozilla

#define extend_seamonkey Расширение SeaMonkey

# LOCALIZATION NOTE (seamonkey_addons):
# link title for https://addons.mozilla.org/en-US/seamonkey/
#define seamonkey_addons Дополнения SeaMonkey

# LOCALIZATION NOTE (seamonkey_themes):
# link title for https://addons.thunderbird.net/en-US/seamonkey/themes
#define seamonkey_themes Темы SeaMonkey

# LOCALIZATION NOTE (seamonkey_dictionaries):
# link title for https://addons.thunderbird.net/en-US/seamonkey/dictionaries
#define seamonkey_dictionaries Словари проверки орфографии

# LOCALIZATION NOTE (seamonkey_plugins):
# link title for https://addons.thunderbird.net/en-US/seamonkey/plugins
#define seamonkey_plugins Плагины для SeaMonkey

#define community_support Сообщество и поддержка

# LOCALIZATION NOTE (seamonkey_community):
# link title for https://www.seamonkey-project.org/community
#define seamonkey_community Сообщество SeaMonkey

# LOCALIZATION NOTE (mozillazine):
# link title for http://www.mozillazine.org/
#define mozillazine mozillaZine

# LOCALIZATION NOTE (seamonkey_support):
# link title for the mozillaZine SeaMonkey Support forum
#define seamonkey_support Форум поддержки SeaMonkey (mozillaZine)

# LOCALIZATION NOTE (seamonkey_l10n):
# insert full bookmark line for localized SeaMonkey page (personal toolbar)
# e.g. #define seamonkey_l10n <DT><A HREF="https://www.seamonkey.tlh/">SeaMonkey tlhIngan</a>
#define seamonkey_l10n <DT><A HREF="https://mozilla-russia.org/products/seamonkey/">Проект SeaMonkey</a> 

# LOCALIZATION NOTE (seamonkey_l10n_long):
# insert full bookmark line for localized SeaMonkey page (normal bookmark)
# e.g. #define seamonkey_l10n_long <DT><A HREF="https://www.seamonkey.tld/">tlhIngan Hol SeaMonkey</a>
#define seamonkey_l10n_long <DT><A HREF="https://mozilla-russia.org/products/seamonkey/">Проект SeaMonkey</a> 

#unfilter emptyLines
