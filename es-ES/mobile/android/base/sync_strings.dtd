<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!ENTITY syncBrand.fullName.label "Firefox Sync">
<!ENTITY syncBrand.shortName.label "Sync">
<!ENTITY sync.title.connect.label "Conectar a &syncBrand.shortName.label;">
<!ENTITY sync.subtitle.connect.label 'Para activar un nuevo dispositivo, seleccione \"Configurar &syncBrand.shortName.label;\" en su dispositivo.'>
<!ENTITY sync.subtitle.pair.label 'Para activarlo, seleccione \"Emparejar un dispositivo\" en su otro dispositivo.'>
<!ENTITY sync.pin.default.label "...\n...\n...\n">
<!ENTITY sync.link.nodevice.label "No tengo el dispositivo conmigo…">
<!ENTITY sync.configure.engines.title.passwords2 "Inicios de sesión">
<!ENTITY sync.configure.engines.title.history "Historial">
<!ENTITY sync.configure.engines.title.tabs "Pestañas">
<!ENTITY sync.default.client.name "&formatS1; en &formatS2;">
<!ENTITY bookmarks.folder.menu.label "Menú de marcadores">
<!ENTITY bookmarks.folder.places.label "">
<!ENTITY bookmarks.folder.tags.label "Etiquetas">
<!ENTITY bookmarks.folder.toolbar.label "Barra de herramientas de marcadores">
<!ENTITY bookmarks.folder.other.label "Otros marcadores">
<!ENTITY bookmarks.folder.desktop.label "Marcadores de escritorio">
<!ENTITY bookmarks.folder.mobile.label "Marcadores del móvil">
<!ENTITY bookmarks.folder.pinned.label "Fijada">
<!ENTITY fxaccount_back_to_browsing "Volver a navegar">
<!ENTITY fxaccount_getting_started_welcome_to_sync "Bienvenido a &syncBrand.shortName.label;">
<!ENTITY fxaccount_getting_started_description2 "Inicie sesión para sincronizar sus pestañas, marcadores, cuentas y más.">
<!ENTITY fxaccount_getting_started_get_started "Comenzar">
<!ENTITY fxaccount_getting_started_old_firefox "¿Usa una versión antigua de &syncBrand.shortName.label;?">
<!ENTITY fxaccount_status_signed_in_as "Conectado como">
<!ENTITY fxaccount_status_manage_account "Administrar cuenta">
<!ENTITY fxaccount_status_auth_server "Servidor de cuentas">
<!ENTITY fxaccount_status_sync_now "Sincronizar ahora">
<!ENTITY fxaccount_status_syncing2 "Sincronizando…">
<!ENTITY fxaccount_status_device_name "Nombre del dispositivo">
<!ENTITY fxaccount_status_sync_server "Servidor de Sync">
<!ENTITY fxaccount_status_sync "&syncBrand.shortName.label;">
<!ENTITY fxaccount_status_sync_enabled "&syncBrand.shortName.label;: activada">
<!ENTITY fxaccount_status_needs_verification2 "Su cuenta necesita ser verificada. Toque para volver a enviar el mensaje de verificación.">
<!ENTITY fxaccount_status_needs_credentials "No se puede conectar. Toque para iniciar sesión.">
<!ENTITY fxaccount_status_needs_upgrade "Necesita actualizar &brandShortName; para iniciar sesión.">
<!ENTITY fxaccount_status_needs_master_sync_automatically_enabled "&syncBrand.shortName.label; está configurado, pero no sincronizando automáticamente. Marque “Sincronizar datos automáticamente” en Ajustes Android &gt; Uso de datos.">
<!ENTITY fxaccount_status_needs_master_sync_automatically_enabled_v21 "&syncBrand.shortName.label; está configurado, pero no sincronizando automáticamente. Marque “Sincronizar datos automáticamente” en el menú Ajustes Android &gt; Uso de datos.">
<!ENTITY fxaccount_status_needs_finish_migrating "Toque para iniciar sesión con su nueva cuenta de Firefox.">
<!ENTITY fxaccount_status_bookmarks "Marcadores">
<!ENTITY fxaccount_status_history "Historial">
<!ENTITY fxaccount_status_passwords2 "Inicios de sesión">
<!ENTITY fxaccount_status_tabs "Abrir pestañas">
<!ENTITY fxaccount_status_legal "Legal">
<!ENTITY fxaccount_status_linktos2 "Términos del Servicio">
<!ENTITY fxaccount_status_linkprivacy2 "Aviso sobre privacidad">
<!ENTITY fxaccount_status_more "Más&ellipsis;">
<!ENTITY fxaccount_remove_account "Desconectar&ellipsis;">
<!ENTITY fxaccount_remove_account_dialog_title2 "¿Desconectar de Sync?">
<!ENTITY fxaccount_remove_account_dialog_message2 "Sus datos de navegación permanecerán en este dispositivo, pero ya no se sincronizarán con su cuenta.">
<!ENTITY fxaccount_remove_account_toast2 "Cuenta de Firefox &formatS; desconectada.">
<!ENTITY fxaccount_remove_account_dialog_action_confirm "Desconectar">
<!ENTITY fxaccount_enable_debug_mode "Activar modo de depuración">
<!ENTITY fxaccount_account_type_label "Firefox">
<!ENTITY fxaccount_options_title "Opciones de &syncBrand.shortName.label;">
<!ENTITY fxaccount_options_configure_title "Configurar &syncBrand.shortName.label;">
<!ENTITY fxaccount_remote_error_UPGRADE_REQUIRED "Necesita actualizar Firefox">
<!ENTITY fxaccount_remote_error_ATTEMPT_TO_CREATE_AN_ACCOUNT_THAT_ALREADY_EXISTS_2 "La cuenta ya existe. &formatS1;">
<!ENTITY fxaccount_remote_error_ATTEMPT_TO_ACCESS_AN_ACCOUNT_THAT_DOES_NOT_EXIST "Dirección de correo o contraseña no válidos">
<!ENTITY fxaccount_remote_error_INCORRECT_PASSWORD "Dirección de correo o contraseña no válidos">
<!ENTITY fxaccount_remote_error_ATTEMPT_TO_OPERATE_ON_AN_UNVERIFIED_ACCOUNT "La cuenta no está verificada">
<!ENTITY fxaccount_remote_error_CLIENT_HAS_SENT_TOO_MANY_REQUESTS "Servidor ocupado, inténtelo de nuevo en poco tiempo">
<!ENTITY fxaccount_remote_error_SERVICE_TEMPORARILY_UNAVAILABLE_TO_DUE_HIGH_LOAD "Servidor ocupado, inténtelo de nuevo en poco tiempo">
<!ENTITY fxaccount_remote_error_UNKNOWN_ERROR "Hubo un problema">
<!ENTITY fxaccount_remote_error_ACCOUNT_LOCKED "La cuenta está bloqueada. &formatS1;">
<!ENTITY fxaccount_sync_sign_in_error_notification_title2 "&syncBrand.shortName.label; no está conectada">
<!ENTITY fxaccount_sync_sign_in_error_notification_text2 "Toque para iniciar sesión como &formatS;">
<!ENTITY fxaccount_sync_finish_migrating_notification_title "¿Terminar actualización de &syncBrand.shortName.label;?">
<!ENTITY fxaccount_sync_finish_migrating_notification_text "Toque para iniciar sesión como &formatS;">
