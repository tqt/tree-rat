<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/.  -->
<!ENTITY aboutDialog.title          "&brandFullName;-ის შესახებ">

<!-- LOCALIZATION NOTE (update.*):
# These strings are also used in the update pane of preferences.
# See about:preferences#advanced.
-->
<!-- LOCALIZATION NOTE (update.checkForUpdatesButton.*, update.updateButton.*):
# Only one button is present at a time.
# The button when displayed is located directly under the Firefox version in
# the about dialog (see bug 596813 for screenshots).
-->
<!ENTITY update.checkForUpdatesButton.label       "განახლებების შემოწმება">
<!ENTITY update.checkForUpdatesButton.accesskey   "გ">
<!ENTITY update.updateButton.label3               "&brandShorterName;-ის გასაახლებლად გაუშვით ხელახლა">
<!ENTITY update.updateButton.accesskey            "ხ">


<!-- LOCALIZATION NOTE (warningDesc.version): This is a warning about the experimental nature of Nightly and Aurora builds. It is only shown in those versions. -->
<!ENTITY warningDesc.version        "&brandShortName; საცდელია და შესაძლოა არამდგრადი იყოს.">
<!-- LOCALIZATION NOTE (warningDesc.telemetryDesc): This is a notification that Nightly/Aurora builds automatically send Telemetry data back to Mozilla. It is only shown in those versions. "It" refers to brandShortName. -->
<!ENTITY warningDesc.telemetryDesc  "იგი ავტომატურად უგზავნის &vendorShortName; -ს ინფორმაციას წარმადობის, აპარატურის, მორგებისა და გამოყენების შესახებ &brandShortName; -ის გასაუმჯობესებლად.">

<!-- LOCALIZATION NOTE (community.exp.*) This paragraph is shown in "experimental" builds, i.e. Nightly and Aurora builds, instead of the other "community.*" strings below. -->
<!ENTITY community.exp.start        "">
<!-- LOCALIZATION NOTE (community.exp.mozillaLink): This is a link title that links to http://www.mozilla.org/. -->
<!ENTITY community.exp.mozillaLink  "&vendorShortName;">
<!ENTITY community.exp.middle       " არის ">
<!-- LOCALIZATION NOTE (community.exp.creditslink): This is a link title that links to about:credits. -->
<!ENTITY community.exp.creditsLink  "გლობალური ერთობა">
<!ENTITY community.exp.end          " რომელიც მუშაობს იმისთვის, რომ ინტერნეტი დარჩეს გახსნილი, საჯარო და ყველასთვის ხელმისაწვდომი.">

<!ENTITY community.start2           "&brandShortName; შექმნილია">
<!-- LOCALIZATION NOTE (community.mozillaLink): This is a link title that links to http://www.mozilla.org/. -->
<!ENTITY community.mozillaLink      "&vendorShortName;">
<!ENTITY community.middle2          " — ">
<!-- LOCALIZATION NOTE (community.creditsLink): This is a link title that links to about:credits. -->
<!ENTITY community.creditsLink      "გლობალური ერთობა">
<!ENTITY community.end3             " რომელიც მუშაობს იმისთვის, რომ ინტერნეტი დარჩეს გახსნილი, საჯარო და ყველასთვის ხელმისაწვდომი.">

<!ENTITY helpus.start               "გსურთ დახმარება? ">
<!-- LOCALIZATION NOTE (helpus.donateLink): This is a link title that links to https://sendto.mozilla.org/page/contribute/Give-Now?source=mozillaorg_default_footer&ref=firefox_about&utm_campaign=firefox_about&utm_source=firefox&utm_medium=referral&utm_content=20140929_FireFoxAbout. -->
<!ENTITY helpus.donateLink          "გააკეთეთ შემოწირულება">
<!ENTITY helpus.middle              " ან ">
<!-- LOCALIZATION NOTE (helpus.getInvolvedLink): This is a link title that links to http://www.mozilla.org/contribute/. -->
<!ENTITY helpus.getInvolvedLink     "შემოგვიერთდი!">
<!ENTITY helpus.end                 "">

<!ENTITY releaseNotes.link          "რა არის ახალი">

<!-- LOCALIZATION NOTE (bottomLinks.license): This is a link title that links to about:license. -->
<!ENTITY bottomLinks.license        "ლიცენზირების შესახებ">

<!-- LOCALIZATION NOTE (bottomLinks.rights): This is a link title that links to about:rights. -->
<!ENTITY bottomLinks.rights         "მომხმარებლის უფლებები">

<!-- LOCALIZATION NOTE (bottomLinks.privacy): This is a link title that links to https://www.mozilla.org/legal/privacy/. -->
<!ENTITY bottomLinks.privacy        "პირადი მონაცემების დაცვის დებულება">

<!-- LOCALIZATION NOTE (update.checkingForUpdates): try to make the localized text short (see bug 596813 for screenshots). -->
<!ENTITY update.checkingForUpdates  "მიმდინარეობს განახლებების შემოწმება...">
<!-- LOCALIZATION NOTE (update.noUpdatesFound): try to make the localized text short (see bug 596813 for screenshots). -->
<!ENTITY update.noUpdatesFound      "&brandShortName; უახლესი ვერსიაა">
<!-- LOCALIZATION NOTE (update.adminDisabled): try to make the localized text short (see bug 596813 for screenshots). -->
<!ENTITY update.adminDisabled       "განახლებები აკრძალულია თქვენი სისტემური ადმინისტრატორის მიერ">
<!-- LOCALIZATION NOTE (update.otherInstanceHandlingUpdates): try to make the localized text short -->
<!ENTITY update.otherInstanceHandlingUpdates "&brandShortName;-ის განახლება უკვე მიმდინარეობს">
<!ENTITY update.restarting          "ეშვება ხელახლა…">

<!-- LOCALIZATION NOTE (update.failed.start,update.failed.linkText,update.failed.end):
     update.failed.start, update.failed.linkText, and update.failed.end all go into
     one line with linkText being wrapped in an anchor that links to a site to download
     the latest version of Firefox (e.g. http://www.firefox.com). As this is all in
     one line, try to make the localized text short (see bug 596813 for screenshots). -->
<!ENTITY update.failed.start        "განახლება ვერ მოხერხდა. ">
<!ENTITY update.failed.linkText     "უახლესი ვერსიის ჩამოტვირთვა">
<!ENTITY update.failed.end          "">

<!-- LOCALIZATION NOTE (update.manual.start,update.manual.end): update.manual.start and update.manual.end
     all go into one line and have an anchor in between with text that is the same as the link to a site
     to download the latest version of Firefox (e.g. http://www.firefox.com). As this is all in one line,
     try to make the localized text short (see bug 596813 for screenshots). -->
<!ENTITY update.manual.start        "განახლებები ხელმისაწვდომია ">
<!ENTITY update.manual.end          "">

<!-- LOCALIZATION NOTE (update.unsupported.start,update.unsupported.linkText,update.unsupported.end):
     update.unsupported.start, update.unsupported.linkText, and
     update.unsupported.end all go into one line with linkText being wrapped in
     an anchor that links to a site to provide additional information regarding
     why the system is no longer supported. As this is all in one line, try to
     make the localized text short (see bug 843497 for screenshots). -->
<!ENTITY update.unsupported.start    "თქვენ ამ სისტემაზე დამატებითი განახლებების შესრულებაც შეგიძლიათ. ">
<!ENTITY update.unsupported.linkText "გაიგეთ მეტი">
<!ENTITY update.unsupported.end      "">

<!-- LOCALIZATION NOTE (update.downloading.start,update.downloading.end): update.downloading.start and
     update.downloading.end all go into one line, with the amount downloaded inserted in between. As this
     is all in one line, try to make the localized text short (see bug 596813 for screenshots). The — is
     the "em dash" (long dash).
     example: Downloading update — 111 KB of 13 MB -->
<!ENTITY update.downloading.start   "განახლების ჩამოტვირთვა — ">
<!ENTITY update.downloading.end     "">

<!ENTITY update.applying            "განახლების გააქტიურება...">

<!-- LOCALIZATION NOTE (channel.description.start,channel.description.end): channel.description.start and
     channel.description.end create one sentence, with the current channel label inserted in between.
     example: You are currently on the _Stable_ update channel. -->
<!ENTITY channel.description.start  "თქვენ ამჟამად იყენებთ ">
<!ENTITY channel.description.end    " განახლების არხს. ">
